# Blog

A small blog as training for programming and editing skills with Neovim.
Forked from [Mangamaui](https://github.com/Mangamaui/eleventy-not-so-minimal-blog-starter) for its beautiful design.

## Getting Started

### 1. Clone this Repository

```
git clone git@gitlab.com:artqmo/blog.git
```

### 2. Navigate to the directory

```
cd your-blog-name
```

### 3. Remove and add your remote origin

```
git remote remove origin
```

```
git remote add origin <the new repository>
```

```
git branch --set-upstream-to=origin/main main
```

### 4. Install dependencies

```
npm install
```

### 5. Edit _data/metadata.json

### 6. Run Eleventy

```
npm start
```

In debug mode:
```
DEBUG=* npx eleventy
```

To build
```
npm run build
```
