---
layout: article
title: Installing Neovim on Windows
date: 2022-06-11
category: "Windows"
---

Sometimes you are in a corporate environment where you can only use Windows.
Additionally it's possible that you do not have admin access on your machine because of corporate policy.
In that case you have to install and use the tools that you have.

In all other cases I recommend using Linux or WSL as there are always drawbacks in a pure Windows installation.

<!-- excerpt -->

## Install software

To easily install, uninstall and manage software packages for programming,
I was pleasantly surprised by [scoop](https://scoop.sh/).

A good starting base is:

- git
- neovim
- nodejs (for powerful code completion and checking via the plugin coc)
- ripgrep (rewrite of grep in rust, needed for the neovim Telescope plugin)

Scoop automatically puts these programs in your path for usage by other programs.

## Configure software

1. In order to use neovim on Windows you have to copy the nvim and coc configuration files into `~\AppData\Local\`.
2. To use nvim in the Windows terminal you have to disable certain keyboard mappings like ctrl-v in the terminal settings.
3. Afterwards you can open nvim in the Windows terminal (PowerShell).
4. Excecute the following commands to install the plugins.
   First `:PlugInstall` then close nvim and reopen it again and do `:CocUpdate`.
5. Now you should have set everything up to start with Pyton programming and writing markdown.
6. For Linux like commands and Git you can use the Git Bash provided together with the Git installation on Windows.

## Current problems

1. The coc-ltex spellchecker is not working as there is an error in the Nodejs installation for that plugin.
