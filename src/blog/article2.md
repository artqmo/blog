---
layout: article
title: Installing Neovim in WSL 
date: 2022-06-12
category: "WSL"
---

To get that Linux feeling with Neovim on Windows we are using the WSL.
Be ready for some crazy installation and ohhh the best feeling after completion.
<!-- excerpt -->

## Install Linux on Windows

- I am using Windows Terminal which has nice support for recognizing WSL images.
- You can install the Windows Terminal via the Microsoft Store as well.
- Given that you have Windows 11 WSL2 is already the standard.
You can install WSL2 via `wsl --install`.
This will install the latest Ubuntu LTS.
- Sadly the Ubuntu LTS has rather old packages.
Especially Nodejs is so old that we can't use it for the Neovim plugin coc.
So we will uninstall Ubuntu with `wsl --unregister Ubuntu`.
- Instead Ubuntu we use Alpine on WSL.
Alpine has a nice image in the Microsoft Store that we will use.
Install Alpine WSL and afterwards click open.
This will open a terminal which will download the latest Alpine version and start it up.

## Configure Alpine

- Enter your username.
The password you should set afterwards is currently the password for root and not the user.
- Now become root with `su` and with the above password to install packages and add a password for the user.
- `apk add sudo curl neovim git ripgrep openssh nodejs npm python3`.
- `passwd {your username}` and enter password for your user.
- Ensure you are on the latest stable release.
For that you have to edit the repositories.
Run `setup-apkrepos` and choose the last option e to edit the repositories with a text editor.
There you now replace the current version like `v3.15` with `latest-stable` and save with `:wq` if you are using vi as your text editor.
Afterwards run `apk update` followed by apk upgrade --available.
- After the update you should do `sync` and then restart the image with `reboot`.
- Now you can check the new version with `cat /etc/alpine-release`.
- You can also further configure your Alpine Linux further for example by adding sudo etc.
For more details have a look here. [Post installation recommendations for Alpine](https://docs.alpinelinux.org/user-handbook/0.1a/Working/post-install.html)
- Now switch back to your user with `su {your username}`.

## Setting up Neovim

- Some keyboard shortcuts have to be deleted in the Windows Terminal preferences like ctrl-c or ctrl-v as they are needed in Neovim.
- Download the dotfiles from GitLab.
- Download and install Vim-Plug: `sh -c 'curl -fLo "${XDG_DATA_HOME:-$HOME/.local/share}"/nvim/site/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'`
- Copy the nvim and coc folders into `~/.config/`
- Move to the folder `~/.config/coc/extensions/`.
In this folder we already copied the package.json for Nodejs.
In order to install these packages we run `npm install`.
This command will take the package.json and install its contents.
- Run `nvim` to open Neovim.
- In Neovim run `:PlugInstall`
- Now close Neovim with `:q!` and restart it.
- Everything should be installed now.

## Uninstall and/or reinstall Alpine

- You can uninstall Alpine with `wsl --unregister Alpine`.
- To reinstall simply click again on open at the Alpine WSL page in the Microsoft Store.

## Open Issues

- Spell checker coc-ltex not working in Windows and WSL.

